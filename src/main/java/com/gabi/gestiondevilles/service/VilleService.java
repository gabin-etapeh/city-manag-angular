package com.gabi.gestiondevilles.service;

import com.gabi.gestiondevilles.entity.Ville;
import com.gabi.gestiondevilles.repository.VilleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class VilleService {

    private final VilleRepository villeRepository;

    public Ville addVille(Ville ville){
        return villeRepository.save(ville);
    }

    public Ville getVille (Integer id){
        return villeRepository.findById(id)
                .orElse(null);
    }
    public List<Ville> getVilles() {
        return villeRepository.findAll();
    }
    public void deleteVille(Integer id){
        villeRepository.deleteById(id);
    }

    public Optional<Ville> updateVille (Integer id, Ville nouvelleVille){
        Optional<Ville> ancienneVillleOptional = villeRepository.findById(id);
        if(ancienneVillleOptional.isPresent()){

            Ville ancienneVille = ancienneVillleOptional.get();
            ancienneVille.setNomVille(nouvelleVille.getNomVille());
            ancienneVille.setDescription(nouvelleVille.getDescription());
            ancienneVille.setDateCreation(nouvelleVille.getDateCreation());
            ancienneVille.setPopulation(nouvelleVille.getPopulation());
            ancienneVille.setSuperficie(nouvelleVille.getSuperficie());
            return Optional.of(villeRepository.save(ancienneVille));
        } else {
            return Optional.empty();
        }
    }
}
