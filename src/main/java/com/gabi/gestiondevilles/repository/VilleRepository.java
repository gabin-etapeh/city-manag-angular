package com.gabi.gestiondevilles.repository;

import com.gabi.gestiondevilles.entity.Ville;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VilleRepository extends JpaRepository<Ville, Integer> {

}
