package com.gabi.gestiondevilles.controler;

import com.gabi.gestiondevilles.entity.Ville;
import com.gabi.gestiondevilles.service.VilleService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
public class VilleController {

    private final VilleService villeService;

    @GetMapping("/ville/{id}")
    public ResponseEntity<Ville> getVille (@PathVariable Integer id){
        return new ResponseEntity<>(villeService.getVille(id), HttpStatus.OK);
    }

    @PostMapping("/ville")
    public ResponseEntity<Ville> addVille (@RequestBody Ville ville) {
        return new ResponseEntity<>(villeService.addVille(ville), HttpStatus.CREATED);
    }

    @GetMapping("/villes")
    public ResponseEntity<List<Ville>> getVilles() {
        return new ResponseEntity<>(villeService.getVilles(), HttpStatus.OK);
    }
    @DeleteMapping(value = "/ville/{id}")
    public ResponseEntity<Integer> deleteVille(@PathVariable Integer id) {
        villeService.deleteVille(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    @PutMapping("/ville/{id}")
    public ResponseEntity<?> updateVille(@PathVariable Integer id,
                                          @RequestBody Ville nouvelleVille){
        Optional<Ville> updatedVille = villeService.updateVille(id, nouvelleVille);
        if(updatedVille.isPresent()){
            return new ResponseEntity<>(updatedVille.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Aucune ville à modifier", HttpStatus.NOT_FOUND);
        }
    }
}
